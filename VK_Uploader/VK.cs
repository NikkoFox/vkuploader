﻿using VkNet.Model;
using VkNet;
using VkNet.Enums.Filters;
using VkNet.Model.RequestParams;
using VkNet.Utils;
using System;
using Newtonsoft.Json.Linq;
using System.Windows;
using System.Net.Http;
using System.Threading.Tasks;
using System.IO;
using System.Net;

namespace VK_Uploader
{
    public class VK : MainWindow
    {
        private static readonly HttpClient httpClient = new HttpClient();
        public delegate void ProgressHandler(long bytes, long currentBytes, long totalBytes);

        //Я заимствовал этот кусок кода тут: http://www.cyberforum.ru/csharp-net/thread2251848.html
        class ProgressStreamContent : StreamContent
        {
            private const int DEFAULT_BUFFER_SIZE = 4096;

            public event ProgressHandler ProgressChanged = delegate { };

            private long currentBytes = 0;
            private long totalBytes = -1;


            public Stream InnerStream { get; }
            public int BufferSize { get; }

            public ProgressStreamContent(Stream innerStream, int bufferSize = DEFAULT_BUFFER_SIZE) :
                base(innerStream, bufferSize)
            {
                InnerStream = innerStream ?? throw new ArgumentNullException(nameof(innerStream));
                BufferSize = bufferSize > 0 ? bufferSize : throw new ArgumentOutOfRangeException(nameof(bufferSize));
            }

            private void ResetInnerStream()
            {
                if (InnerStream.Position != 0)
                {
                    // Если внутренний поток нужно считать повторно, то этот внутренний поток должен поддерживать
                    // возврат каретки(например FileStream), иначе внутренний поток не может быть считан повторно
                    // в целевой поток(например NetworkStream)
                    if (InnerStream.CanSeek)
                    {
                        InnerStream.Position = 0;
                        currentBytes = 0;
                    }
                    else
                        throw new InvalidOperationException("The inner stream has already been read!");
                }
            }

            protected override async Task SerializeToStreamAsync(Stream stream, TransportContext context)
            {
                if (stream == null) throw new ArgumentNullException(nameof(stream));

                // Сбрасываем состояние внутреннего потока
                ResetInnerStream();

                // Если общее количество байт еще не получено, то пытаемся получить
                // его из заголовков контента
                if (totalBytes == -1)
                    totalBytes = Headers.ContentLength ?? -1;

                // Если общее количество байт еще не найдено, то пытаемся
                // вычислить его из потока
                if (totalBytes == -1 && TryComputeLength(out var computedLength))
                    totalBytes = computedLength == 0 ? -1 : computedLength;

                // Если общее количество байт отрицательное значение, то
                // присваеваем ему -1, идентифицирующее о невалидном общем количестве байт
                totalBytes = Math.Max(-1, totalBytes);

                // Начинаем читать внутренний поток
                var buffer = new byte[BufferSize];
                var bytesRead = 0;
                while ((bytesRead = await InnerStream.ReadAsync(buffer, 0, buffer.Length)) != 0)
                {
                    stream.Write(buffer, 0, bytesRead);
                    currentBytes += bytesRead;

                    // Генерируем событие ProgressChanged, чтобы оповестить о текущем прогрессе считывания
                    ProgressChanged(bytesRead, currentBytes, totalBytes);
                }
            }

            protected override bool TryComputeLength(out long length)
            {
                var result = base.TryComputeLength(out length);
                totalBytes = length;
                return result;
            }

            protected override void Dispose(bool disposing)
            {
                if (disposing)
                    InnerStream.Dispose();

                base.Dispose(disposing);
            }
        }
        public static string VK_Authorize(string login, string password, VkApi vk, string token = "")
        {
            if (login != "" && password != "")
            {
                vk.Authorize(new ApiAuthParams
                {
                    ApplicationId = 6752790,
                    Login = login,
                    Password = password,
                    Settings = Settings.Offline | Settings.Groups | Settings.Wall | Settings.Photos
                });
            }
            else
            {
                vk.Authorize(new ApiAuthParams
                {
                    AccessToken = token
                });
            }
            return vk.Token;
        }
        public static string VK_AuthorizeTwoFactor(string login, string password, VkApi vk, string token = "")
        {
            if (login != "" && password != "")
            {
                vk.Authorize(new ApiAuthParams
                {
                    ApplicationId = 6752790,
                    Login = login,
                    Password = password,
                    Settings = Settings.Offline | Settings.Groups | Settings.Wall | Settings.Photos,
                    TwoFactorAuthorization = () =>
                    {
                        return TwoFactorAuthorization();
                    }
                });
            }
            else
            {
                vk.Authorize(new ApiAuthParams
                {
                    AccessToken = token
                });
            }
            return vk.Token;
        }
        private static string TwoFactorAuthorization()
        {
            Captcha code = new Captcha();
            string auth_code;
            if (code.ShowDialog() == true)
            {
                auth_code = code.CodeOrCaptcha;
                return auth_code;
            }
            else
            {
                MessageBox.Show("Ошибка кода\n" +
           "Попробуйте заново", "Ошибка",
           MessageBoxButton.OK, MessageBoxImage.Error);
                //VK_Authorize(login, password, vk);
                return "";
            }
        }
        public static VkCollection<Group> Groups_Get(VkApi vk)
        {
            var groups = vk.Groups.Get(new GroupsGetParams() { Count = 1000, Extended = true, Filter = GroupsFilters.Editor });
            return groups;
        }
        public static VkCollection<PhotoAlbum> PhotoAlbums_Get(long Owner_id, VkApi vk)
        {
            var photoalbums = vk.Photo.GetAlbums(new PhotoGetAlbumsParams() { OwnerId = Owner_id });
            return photoalbums;
        }
        public static void Upload_Photo(byte[] img, long group_id, long album_id, string caption, VkApi vk)
        {
            Stream stream = new MemoryStream(img);
            var streamContent = new ProgressStreamContent(stream);
            streamContent.ProgressChanged += (bytes, currBytes, totalBytes) => App.Current.Dispatcher.Invoke(
                delegate { ProgressBarImg.Value = ((double)currBytes / totalBytes * 100); UploadPerc.Content = Math.Round((double)currBytes / totalBytes * 100).ToString() + "%"; });
            MultipartFormDataContent form = new MultipartFormDataContent
            {
                { streamContent, "photo", "photo.jpg" }
            };
            var uploadServer = vk.Photo.GetUploadServer(albumId: album_id, groupId: group_id);
            var response = httpClient.PostAsync(uploadServer.UploadUrl, form).Result;
            var responseFile = response.Content.ReadAsStringAsync().Result;
            dynamic data = JObject.Parse(responseFile);
            while (data.photos_list == "null" || data.photos_list == "[]")
            {
                form = new MultipartFormDataContent
                {
                    { streamContent, "photo", "photo.jpg" }
                };
                uploadServer = vk.Photo.GetUploadServer(albumId: album_id, groupId: group_id);
                response = httpClient.PostAsync(uploadServer.UploadUrl, form).Result;
                responseFile = response.Content.ReadAsStringAsync().Result;
                data = JObject.Parse(responseFile);
            }

            var photos = vk.Photo.Save(new PhotoSaveParams
            {
                SaveFileResponse = responseFile,
                AlbumId = album_id,
                GroupId = group_id,
                Caption = caption
            });
        }

    }
}
