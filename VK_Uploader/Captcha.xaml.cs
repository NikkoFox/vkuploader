﻿using System;
using System.Windows;

namespace VK_Uploader
{
    /// <summary>
    /// Interaction logic for captcha.xaml
    /// </summary>
    public partial class Captcha : Window
    {
        public Captcha()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            Code.SelectAll();
            Code.Focus();
        }
        public string CodeOrCaptcha
        {
            get { return Code.Text; }
        }
    }
}
